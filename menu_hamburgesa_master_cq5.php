<?php 
error_reporting(E_ERROR | E_PARSE);
################################FUNCIONES FUNCIONES#####################################################
function jsonp_decode($jsonp, $assoc = false) { // PHP 5.3 adds depth as third parameter to json_decode
   $jsonLiterals = array('true' => 1, 'false' => 1, 'null');
   if(preg_match('/^[^[{"\d]/', $jsonp) && !isset($jsonLiterals[$jsonp])) { // we have JSONP
      $jsonp = substr($jsonp, strpos($jsonp, '('));
   }
   return json_decode(trim($jsonp,'();'), $assoc);
}

function chars_specials($str){

  $str = str_replace( '&quot;', '', $str ); 
  $str = str_replace( '&nbsp;', '', $str );  
  $str = str_replace( '&ldquo;', '', $str );
  $str = str_replace( '&rdquo;', '', $str );
  $str = str_replace( "&lsquo;", "'", $str );
  $str = str_replace( "&rsquo;", "'", $str ); 
  
  return $str; 
}


#########################################################################################################
$urls = "http://troya.esmas.com.mx:4503/bin/shared/hamburgerMenu.deportes.json";
$ch1 = curl_init();
curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch1, CURLOPT_URL, $urls);
$data = curl_exec($ch1);
$datas = jsonp_decode($data);
if (isset($datas)) {
	$items = array();
	$items['vertical'] = $datas->vertical;
	if (isset($datas->link)) {
		$items['link'] = $datas->link;
	}else{
		$items['link'] = "";
	}
	if (isset($datas->items)) {
		$nUno = $datas->items;
		foreach ($nUno as $keyN1 => $value) {
			$items['items'][$keyN1]['title'] = chars_specials($value->title);
			if (isset($value->link)) {
				$items['items'][$keyN1]['link'] = $value->link;
			}else{
				$items['items'][$keyN1]['link'] = "";
			}
			if (isset($value->items)) {
				foreach ($value->items as $keyN2 => $valu) {
					$items['items'][$keyN1]['items'][$keyN2]['title'] = chars_specials($valu->title);
					if (isset($valu->link)) {
						$items['items'][$keyN1]['items'][$keyN2]['link'] = $valu->link;
					}else{
						$items['items'][$keyN1]['items'][$keyN2]['link'] = "";
					}
					if (isset($valu->items)) {
						foreach ($valu->items as $keyN3 => $val) {
							$items['items'][$keyN1]['items'][$keyN2]['items'][$keyN3]['title'] = chars_specials($val->title);
							if (isset($val->link)) {
								$items['items'][$keyN1]['items'][$keyN2]['items'][$keyN3]['link'] = $val->link;
							}else{
								$items['items'][$keyN1]['items'][$keyN2]['items'][$keyN3]['link'] = "";
							}
							if (isset($val->items)) {
								foreach ($val->items as $keyN4 => $v) {
									$items['items'][$keyN1]['items'][$keyN2]['items'][$keyN3]['items'][$keyN4]['title'] = chars_specials($v->title);
									if (isset($v->link)) {
										$items['items'][$keyN1]['items'][$keyN2]['items'][$keyN3]['items'][$keyN4]['link'] = $v->link;
									}else{
										$items['items'][$keyN1]['items'][$keyN2]['items'][$keyN3]['items'][$keyN4]['link'] = "";
									}
									
								}
							}
						}
					}
				}
			}
		}
	}
	echo "jsonHamburguer(".json_encode($items).")";	
}else{

}

 ?>